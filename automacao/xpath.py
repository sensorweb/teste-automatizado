#xpath das paginas site
xpath_SITE_LINK = "https://sensorweb.com.br"  
xpath_clientes = '//*[@id="menu-item-46"]/a/span[2]'
xpath_sobrenos = '//*[@id="menu-item-94"]/a'
xpath_contatos = '//*[@id="menu-item-550"]/a' 
xpath_blog = '//*[@id="menu-item-89"]/a'   
xpath_parceiros = '//*[@id="menu-item-93"]/a'   
xpath_solucoes = '//*[@id="menu-item-91"]/a'
xpath_home = '//*[@id="menu-item-82"]/a' 

# elemnetos na home
xpath_vantagens = '//*[@id="av-custom-submenu-1"]/li[1]'
xpath_como_funciona = '//*[@id="av-custom-submenu-1"]/li[2]/a/span[2]'
xpath_area_atuacao = '//*[@id="av-custom-submenu-1"]/li[3]/a/span[2]'
xpath_oque_dizem = '//*[@id="av-custom-submenu-1"]/li[4]/a/span[2]'

# interação na home
xpath_video = '//*[@id="funcionalidades-solucao"]/div/div/div/div/div[3]/div/div'
xpath_Home_banco_sangue = '//*[@id="av-tab-section-1"]/div/div[1]/a[2]'
xpath_Home_laboratorio_pesquisa = '//*[@id="av-tab-section-1"]/div/div[1]/a[3]'
xpath_Home_logistica_farma = '//*[@id="av-tab-section-1"]/div/div[1]/a[4]'

#formulario entre em contato
xpath_form_username = '//*[@id="rd-text_field-kqhgpZENZlmb7ThC2Dbo0Q"]' 
xpath_form_email = '//*[@id="rd-email_field-QbPb52n6msZrGi_CIfdPKw"]' 
xpath_form_empresa = '//*[@id="rd-text_field-3m5nZYUM1HV5X6qbaRPFrg"]' 
xpath_form_telefone = '//*[@id="rd-phone_field-lbE-L3BllD-dYsU5dSmv9A"]' 
xpath_form_cargo = '//*[@id="rd-text_field-Oc9lZxCXW1XViYTmUIhu8Q"]' 
xpath_form_atuacao = '//*[@id="rd-select_field-VK36zfgSnMr9WVbIijelIQ"]' 
xpath_form_mensagem = '//*[@id="rd-text_area_field-fbdMAzrk4p2fJVVF7WjuAg"]' 
xpath_form_checkbox = '//*[@id="conversion-form-contato-lgpd"]/div[1]/div[8]/div/label/input'
xpath_form_submit = '//*[@id="rd-button-kwwh1tsk"]' 
      
#elemnetos extras a serem usados         
xpath_video = '//*[@id="funcionalidades-solucao"]/div/div/div/div/div[3]/div/div'
xpath_elements = '//*[@id="av_section_1"]/div/a'

